package assafPatishi.MyMovies.Data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;
import static assafPatishi.MyMovies.Data.MoviesDbConstants.*;

public class MoviesHandler {
	
	private MoviesDbHelper dbHelper;
	
	
	public MoviesHandler(Context context){
		dbHelper = new MoviesDbHelper(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	
	public Cursor getAllMovies(){
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		
		Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, MOVIE_SUBJECT); //Sort the query by movie title in ascending order
		return cursor;
	}
	
	
	public Cursor getAllMoviesBySearch(String search){ //searching by keyword
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		
		Cursor cursor = db.query(TABLE_NAME, null, MOVIE_SUBJECT + " LIKE " + "'%"+search+"%'", null, null, null,MOVIE_SUBJECT);
		return cursor;
	}


	
	public Movie getMovie(int id){
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		Movie movie = null;
		
		Cursor cursor = db.query(TABLE_NAME, null, MOVIE_ID + "=?", new String[] {String.valueOf(id)}, null, null, null, null);
		
		if(cursor.moveToFirst()){ 
			movie = new Movie();
			movie.setId(cursor.getInt(0));
			movie.setSubject(cursor.getString(1));
			movie.setBody(cursor.getString(2));
			movie.setUri(cursor.getString(3));
		}
		return movie;
	}
	
	
	public void addMovie(Movie movie){
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		
		ContentValues newMovieValues = new ContentValues();
		newMovieValues.put(MOVIE_SUBJECT, movie.getSubject());
		newMovieValues.put(MOVIE_BODY, movie.getBody());
		newMovieValues.put(MOVIE_IMAGE_URI, movie.getUri());
		
		try{
			db.insertOrThrow(TABLE_NAME, null, newMovieValues);
		}
		catch(SQLiteException ex){
			throw ex;
		}
		finally{
			db.close();
		}
	}
	
	
	public void updateMovie(Movie movie){
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		
		try{
			ContentValues values = new ContentValues();
			values.put(MOVIE_SUBJECT, movie.getSubject());
			values.put(MOVIE_BODY, movie.getBody());
			values.put(MOVIE_IMAGE_URI, movie.getUri());
			
			db.update(TABLE_NAME, values, MOVIE_ID + "=?", new String[] {String.valueOf(movie.getId())});
		}
		catch(SQLiteException ex){
			throw ex;
		}
		finally{
			db.close();
		}
	}
	
	
	public void deleteMovie(Movie movie){
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		
		try{
			db.delete(TABLE_NAME, MOVIE_ID + "=?", new String[] {String.valueOf(movie.getId())});
		}
		catch(SQLiteException ex){
			throw ex;
		}
		finally{
			db.close();
		}
	}
	
	
	public void deleteAllMovies(){
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		
		try{
			db.delete(TABLE_NAME, null, null);
		}
		catch(SQLiteException ex){
			throw ex;
		}
		finally{
			db.close();
		}
	}

}
