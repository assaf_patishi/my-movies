package assafPatishi.MyMovies.Data;

public class Movie {
	
	private int id;
	private String subject;
	private String body;
	private String uri;
	
	
	public Movie(){
		
	}
	
	
	public void setId(int id){
		this.id = id;
	}
	
	
	public int getId(){
		return id;
	}
	
	
	public void setSubject(String subject){
		this.subject = subject;
	}
	
	
	public String getSubject(){
		return subject;
	}
	
	
	public void setBody(String body){
		this.body = body;
	}
	
	
	public String getBody(){
		return body;
	}
	
	
	public void setUri(String uri){
		this.uri = uri;
	}
	
	
	public String getUri(){
		return uri;
	}
	
	
	

}
