package assafPatishi.MyMovies.Data;

public class MoviesDbConstants {
	
	public static final String DATABASE_NAME = "movies.db";
	public static final int DATABASE_VERSION = 1;
	public static final String TABLE_NAME = "movies";
	
	
	public static final String MOVIE_ID = "_id";
	public static final String MOVIE_SUBJECT = "subject";
	public static final String MOVIE_BODY = "body";
	public static final String MOVIE_IMAGE_URI = "uri";
	
	
	

}
