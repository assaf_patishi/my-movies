package assafPatishi.MyMovies.Data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import static assafPatishi.MyMovies.Data.MoviesDbConstants.*;

public class MoviesDbHelper extends SQLiteOpenHelper {

	public MoviesDbHelper(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
		
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		
		
		String CREATE_MOVIES_TABLE = "CREATE TABLE " + TABLE_NAME + "(" + MOVIE_ID + " INTEGER PRIMARY KEY," + MOVIE_SUBJECT
				+ " TEXT," + MOVIE_BODY + " TEXT," + MOVIE_IMAGE_URI + " TEXT" + ")";
		
		try{
			db.execSQL(CREATE_MOVIES_TABLE);
		}
		catch(SQLiteException ex){
			
		}

		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		
		
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
	
		onCreate(db);
		
	}
	
	

}
