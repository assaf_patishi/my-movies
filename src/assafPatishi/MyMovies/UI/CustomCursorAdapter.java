package assafPatishi.MyMovies.UI;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import assafPatishi.MyMovies.UI.R.color;

/////////////////// The cursor adapter for the main list view. The method bindView() below,responsible for the look of each item in the listView.

class CustomCursorAdapter extends CursorAdapter{
	
	private LayoutInflater inflater;
	private MainActivity activity;

	public CustomCursorAdapter(Context context, Cursor c, MainActivity activity) {
		super(context, c);
		inflater = LayoutInflater.from(context);
		this.activity = activity;
	}
	
	
	public void bindView(View view, Context context, Cursor cursor) {
		//Setting the look and functionality of a single list row in the main movie list
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(activity);
		InputStream in = null;
		ImageView imageView = (ImageView)view.findViewById(R.id.movie_image);
		ImageView markView = (ImageView)view.findViewById(R.id.watched_mark);
		TextView textView = (TextView)view.findViewById(R.id.movie_title);
		TextView ratingText = (TextView)view.findViewById(R.id.rating_text);
		ratingText.setTextSize(activity.getScreenWidth()/2/15);  //setting the rating textView width.  taking half of the screen width divided by 15.
		textView.setText(cursor.getString(1));                  
		textView.setTextColor(Color.BLACK);
		if(cursor.getString(3)==null){
			imageView.setImageResource(R.drawable.no_image);
		}
		else{
			try{
				in = activity.openFileInput(cursor.getString(3));
				Bitmap bitmap = BitmapFactory.decodeStream(in);
				imageView.setImageBitmap(bitmap);
			}
			catch(FileNotFoundException e){} 
			finally{
				try{
					in.close();
				}
				catch(IOException e){}
			}
		}
		if(preferences.getBoolean(String.valueOf(cursor.getInt(0)), false)){
			markView.setVisibility(View.VISIBLE);
		}
		else{
			markView.setVisibility(View.INVISIBLE);
		}
		if(preferences.getInt("movieRating"+String.valueOf(cursor.getInt(0)), 0)>0){
			int rating = preferences.getInt("movieRating"+String.valueOf(cursor.getInt(0)),0);
			StringBuilder builder = new StringBuilder();
			for(int i = 0; i<rating; i++){
				builder.append("*");
				builder.append(" ");
			}
			ratingText.setText(builder.toString());
			//setting the rating textView text color from red to green (green is 10)
			if(rating<=3){
				ratingText.setTextColor(Color.RED);
			}
			else if(rating>3&&rating<=6){
				ratingText.setTextColor(activity.getResources().getColor(color.orange));
			}
			else if(rating>6&&rating<=9){
				ratingText.setTextColor(activity.getResources().getColor(color.yellow));
			}
			else{
				ratingText.setTextColor(activity.getResources().getColor(color.bright_green));
			}
		}
		else{
			ratingText.setText("");
		}
	}
	
	
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		return inflater.inflate(R.layout.list_row,parent,false);
	}

}
