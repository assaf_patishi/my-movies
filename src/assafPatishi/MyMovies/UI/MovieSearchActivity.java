package assafPatishi.MyMovies.UI;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import assafPatishi.MyMovies.Data.MoviesHandler;

public class MovieSearchActivity extends ListActivity implements OnItemClickListener,TextWatcher{
	
	private AutoCompleteTextView txtSearch;
	private InternetSearchTask internetSearchTask;
	private ArrayList<Long>listItemsIds;
	private AutoCompletionTask autoCompletionTask;
	
	@Override
	
	
	
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_internet_search);
		txtSearch = (AutoCompleteTextView)findViewById(R.id.search_edit);
		txtSearch.addTextChangedListener(this);
		ListView lv = getListView();
		lv.setOnItemClickListener(this);
		
		
	}
	


	public void showInternetConnectionErrorMessage(){
		Toast toast = Toast.makeText(this, "Search failed,check the internet connection", Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
	}
	
	
	public void onButtonClick(View view){
		Button b = (Button)view;
		switch(b.getId()){
		case R.id.go_btn:
			if(!TextUtils.isEmpty(txtSearch.getText().toString().trim())){
				internetSearchTask = new InternetSearchTask(this);
				internetSearchTask.execute(txtSearch.getText().toString());
			}
			break;
		case R.id.options:
			openOptionsMenu();
			break;
		case R.id.cancel_internet_search_btn:
			finish();
		}
	}
	
	
	public void processResponse(String response){ //getting the JSON object from the getMovieByIdTask and making a list
		try{
			JSONObject responseObject = new JSONObject(response);
			JSONArray movies = responseObject.getJSONArray("movies");
			ListView lv = getListView();
			if(movies.length()==0 && lv.getCount()==0){
				this.setListAdapter(null);
				Toast toast = Toast.makeText(this, "No results found", Toast.LENGTH_LONG);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
				return;
			}
			else if(movies.length()==0 && lv.getCount()>0){ //If results = 0 but the list view is showing previews results,DON'T set the adapter to null
				Toast toast = Toast.makeText(this, "No results found", Toast.LENGTH_LONG);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
				return;
			}
			ArrayList<String>listItems = new ArrayList<String>();
			listItemsIds = new ArrayList<Long>();
			for(int i = 0; i<movies.length(); i++){
				JSONObject movie = movies.getJSONObject(i);
				String title = movie.getString("title");
				long id = movie.getLong("id");    //The movie id. will be used later in GetMovieByIdTask to get the movie info from the movie search API
				listItems.add(title);
				listItemsIds.add(id);
			}
			ArrayAdapter<String>adapter = new ArrayAdapter<String>(this, R.layout.search_result_list_row, listItems);
			this.setListAdapter(adapter);
		}
		catch(JSONException e){
			
		}
	}


	@Override
	public void onItemClick(AdapterView<?> adView, View view, int position, long id) {
		long itemId = listItemsIds.get(position);  //Getting the specific id from the id ArrayList for the specific item in the list (By the position param')
		Intent intent = new Intent(this, EditMovieActivity.class);
		intent.putExtra("internetMode", true);
		intent.putExtra("itemId", itemId);   
		startActivity(intent);
		
	}



	@Override
	public void afterTextChanged(Editable arg0) {
		
		
	}



	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
			int arg3) {
		
		
	}



	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
		if(arg0.length()>=txtSearch.getThreshold()){
			if(autoCompletionTask!=null&&autoCompletionTask.getStatus()==AsyncTask.Status.FINISHED){
				autoCompletionTask = new AutoCompletionTask(this);
				autoCompletionTask.execute(arg0.toString());
			}
			else if(autoCompletionTask==null){
				autoCompletionTask = new AutoCompletionTask(this);
				autoCompletionTask.execute(arg0.toString());
			}
		}
		
	}
	
	
	public void setTxtSearchAdapter(ArrayList<String>titles){
		ArrayAdapter<String>adapter = new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,titles);
		txtSearch.setAdapter(adapter);
	}




	
	
	
	
	
}
