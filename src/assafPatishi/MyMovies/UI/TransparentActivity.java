package assafPatishi.MyMovies.UI;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.SimpleCursorAdapter;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import assafPatishi.MyMovies.Data.Movie;
import assafPatishi.MyMovies.Data.MoviesHandler;
import static assafPatishi.MyMovies.Data.MoviesDbConstants.*;

//Transparent activity for the search function (Theme.Transparent).  this is a list activity that displays above the main activity when choosing "search" from the options menu

public class TransparentActivity extends ListActivity implements OnItemClickListener,TextWatcher{
	
	private MoviesHandler handler;
	private EditText searchText;
	private String[] cols = {MOVIE_SUBJECT};
	private int[] views = {R.id.movie_title_transparent};
	private Button removeTextBtn;
	
	


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_transparent);
		handler = new MoviesHandler(this);
		ListView lv = getListView();
		lv.setOnItemClickListener(this);
		registerForContextMenu(lv);
		searchText = (EditText)findViewById(R.id.search_query);
		searchText.addTextChangedListener(this);
		removeTextBtn = (Button)findViewById(R.id.remove_text);
		searchText.requestFocus();
		
	}
	
	
	

	@Override
	protected void onResume() {
		super.onResume();

		
	}




	@Override
	public void onItemClick(AdapterView<?> adView, View view, int position, long id) {
		Intent intent = new Intent(this, EditMovieActivity.class);
		intent.putExtra("addNewMode", false);
		String[] extras = new String[4];
		Movie movie = handler.getMovie((int)id);
		extras[0] = movie.getSubject();
		extras[1] = movie.getBody();
		extras[2] = movie.getUri();
		extras[3] = String.valueOf(movie.getId());
		intent.putExtra("extras", extras);
		startActivity(intent);
		
	}
	
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View view, ContextMenuInfo menuInfo){
		super.onCreateContextMenu(menu, view, menuInfo);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.transparent_activity_context, menu);
	}
	
	
	public boolean onContextItemSelected(MenuItem item){
		AdapterContextMenuInfo info = (AdapterContextMenuInfo)item.getMenuInfo();
		deleteSingleItem((int)info.id);
		return true;
	}
	
	
	
	public void onButtonClick(View view){
		Button b = (Button)view;
		switch(b.getId()){
		case R.id.remove_text:
			searchText.setText(null);
			break;
		case R.id.back_btn:
			finish();
		}
	}



	@Override
	public void afterTextChanged(Editable arg0) {
		
		
	}




	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
			int arg3) {
		
		
	}




	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
		showMovies();
		
	}
	
	
	public void showMovies(){
		if(searchText.getText().toString().trim().length()==0){
			this.setListAdapter(null);
			removeTextBtn.setVisibility(View.GONE);
		}
		else{
			Cursor cursor = handler.getAllMoviesBySearch(searchText.getText().toString().trim());
			SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,R.layout.transparent_list_row,cursor,cols,views,0);
			this.setListAdapter(adapter);
			removeTextBtn.setVisibility(View.VISIBLE);
			this.startManagingCursor(cursor);
		}
		
	}
	
	
	public void deleteSingleItem(int id){ 
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
		final SharedPreferences.Editor editor = preferences.edit();
		final int itemId = id;
		final MoviesHandler handler = new MoviesHandler(this);
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle("Delete");
		alertDialog.setMessage("Delete this item?");
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener(){
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Movie movie = new Movie();
				movie.setId(itemId);
				if(movie.getUri()!=null){
					deleteFile(movie.getUri());
				}
				try{
					handler.deleteMovie(movie);
					editor.remove(String.valueOf(itemId));
					editor.remove("movieRating"+String.valueOf(itemId));
					editor.commit();
				}
				catch(SQLiteException ex){
					Toast toast = Toast.makeText(getApplicationContext(), "Failed delete item", Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
				showMovies();
				MainActivity.activity.refreshList();
			}
		});
		alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		alertDialog.show();
		
		
	}
	


	
	
	
	
	

}
