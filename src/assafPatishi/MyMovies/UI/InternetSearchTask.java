package assafPatishi.MyMovies.UI;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import android.app.ProgressDialog;
import android.os.AsyncTask;

public class InternetSearchTask extends AsyncTask<String, Void, String>{
	
	
	private MovieSearchActivity activity;
	private static final String RT_URL = 
			"http://api.rottentomatoes.com/api/public/v1.0/movies.json?apikey=fvttwvswhe5sfvpu4cycrfs8&q=";
	private ProgressDialog progressDialog;
	
	
	public InternetSearchTask(MovieSearchActivity activity){
		this.activity = activity;
	}

	@Override
	protected String doInBackground(String... strings) {
		String response = sendSearchRequest(strings[0]);
		return response;
	}

	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		progressDialog = new ProgressDialog(activity);
		progressDialog.setMessage("Searching for results...");
		progressDialog.setIndeterminate(true);
		progressDialog.setCanceledOnTouchOutside(false);
		progressDialog.show();
	}

	
	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		if(result!=null){
			progressDialog.dismiss();
			activity.processResponse(result);
		}
		else{
			progressDialog.dismiss();
			activity.showInternetConnectionErrorMessage();
		}
	}

	
	@Override
	protected void onProgressUpdate(Void... values) {
		super.onProgressUpdate(values);
	}
	
	
	private String sendSearchRequest(String searchString){
		BufferedReader input = null;
		HttpURLConnection httpCon = null;
		StringBuilder response = new StringBuilder();
		try{
			String query = URLEncoder.encode(searchString, "utf-8");
			URL url = new URL(RT_URL + query);
			httpCon = (HttpURLConnection)url.openConnection();
			if(httpCon.getResponseCode()!=HttpURLConnection.HTTP_OK){
				return null;
			}
			input = new BufferedReader(new InputStreamReader(httpCon.getInputStream()));
			String line;
			while((line = input.readLine())!=null){
				response.append(line + "\n");
			}
		}
		catch(Exception e){}
		finally{
			if(input!=null){
				try{
					input.close();
				}
				catch(IOException e){}
			}
			if(httpCon!=null){
				httpCon.disconnect();
			}
		}
		return response.toString();
			
	}
	


}
