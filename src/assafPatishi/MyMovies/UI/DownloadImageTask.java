package assafPatishi.MyMovies.UI;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import android.app.ProgressDialog;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
	
	private enum TaskStatus{OK,FAIL}
	
	private TaskStatus status;
	private EditMovieActivity activity;
	private ProgressDialog progressDialog;
	
	//	This class responsible for downloading image from url and resizing as necessary
	
	public DownloadImageTask(EditMovieActivity activity){
		this.activity = activity;
		status = TaskStatus.FAIL;
	}

	@Override
	protected Bitmap doInBackground(String... urls) {
		Bitmap bitmap = null;
		BitmapFactory.Options options = new BitmapFactory.Options();
		HttpURLConnection connection = null;
		InputStream in = null;
		try{
			URL url = new URL(urls[0]);
			connection = (HttpURLConnection)url.openConnection();
			if(connection.getResponseCode()!=HttpURLConnection.HTTP_OK){
				return null;
			}
			in = new BufferedInputStream(connection.getInputStream());
			options.inJustDecodeBounds = true;
			bitmap = BitmapFactory.decodeStream(in, null, options); //checking the image raw dimensions
			options.inSampleSize = calculateInSampleSize(options, activity.getImageViewWidth(), activity.getImageViewHeight()); //calculating the proper image dimensions for the activity's image view
			options.inJustDecodeBounds = false;
			in.reset(); //Reseting the input stream in order to decode the bitmap again with the proper options properties
			bitmap = BitmapFactory.decodeStream(in, null, options);
		}
		catch(Exception e){}
		finally{
			if(in!=null){
				try{
					in.close();
				}
				catch(IOException e){}
			}
				if(connection!=null){
					connection.disconnect();
				}
			}
		if(bitmap!=null){
			status = TaskStatus.OK;  //Only when everything went well and the bitmap is not null,the TaskStatus is set to OK.
		}
		return bitmap;
	}
		

		
	

	@Override
	protected void onPostExecute(Bitmap result) {
		super.onPostExecute(result);
		if(status==TaskStatus.FAIL){  //If the Task status = FAIL ,showDownloadErrorMessage() from the main activity,raise a toast to the user.
			progressDialog.dismiss();
			activity.showDownloadErrorMessage();
		}
		else{
			activity.setImage(result); //...else,the image is set to the image view in the edit activity.
			progressDialog.dismiss();
		}
			
		
	}

	@Override
	protected void onProgressUpdate(Void... values) {
		super.onProgressUpdate(values);
		
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		progressDialog = new ProgressDialog(activity);
		progressDialog.setMessage("Downloading image...");
		progressDialog.setIndeterminate(true);
		progressDialog.setCanceledOnTouchOutside(false);
		progressDialog.show();

	}
	
	
	private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
	    int height = options.outHeight;
	    int width = options.outWidth;
	    int inSampleSize = 1;
	    if (height > reqHeight || width > reqWidth) {
	    	// Calculate ratios of height and width to requested height and width
	        final int heightRatio = Math.round((float) height / (float) reqHeight);
	        final int widthRatio = Math.round((float) width / (float) reqWidth);
	        
	        inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
	    }
	    return inSampleSize;
	}
	

	
	
	
	
	
	
	
	

}
