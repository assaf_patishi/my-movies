package assafPatishi.MyMovies.UI;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

//After picking a movie from the internet search list,this Task gathering all that movie info (by the API id number) for displaying in the EditMovieActivity.
//Returns a MovieObject() packed with all the necessary data.
public class GetMovieByIdTask extends AsyncTask<Long, Void, GetMovieByIdTask.MovieObject>{
	private EditMovieActivity activity;
	private ProgressDialog progressDialog;
	private final String FIRST = "http://api.rottentomatoes.com/api/public/v1.0/movies/";
	private final String LAST = ".json?apikey=fvttwvswhe5sfvpu4cycrfs8";
	
	
	
	public GetMovieByIdTask(EditMovieActivity activity){
		this.activity = activity;
	}

	@Override
	protected MovieObject doInBackground(Long... ids) {
		MovieObject currentMovieObject = null;
		BufferedReader input = null;
		HttpURLConnection httpCon = null;
		StringBuilder response = new StringBuilder();
		StringBuilder movieDetails = new StringBuilder();
		String searchString = FIRST+ids[0]+LAST;
		String imageUrl = null;
		try{
			URL url = new URL(searchString);
			httpCon = (HttpURLConnection)url.openConnection();
			if(httpCon.getResponseCode()!=HttpURLConnection.HTTP_OK){
				return null;
			}
			input = new BufferedReader(new InputStreamReader(httpCon.getInputStream()));
			String line;
			while((line = input.readLine())!=null){
				response.append(line + "\n");
			}
		}
		catch(Exception e){}
		finally{
			if(input!=null){
				try{
					input.close();
				}
				catch(IOException e){}
			}
			if(httpCon!=null){
				httpCon.disconnect();
			}
		}
		try{
			JSONObject responseObject = new JSONObject(response.toString());
			JSONArray genres = responseObject.getJSONArray("genres");
			currentMovieObject = new MovieObject();
			currentMovieObject.setSubject(responseObject.getString("title"));
			movieDetails.append("TITLE: ");
			movieDetails.append(currentMovieObject.getSubject());
			movieDetails.append("\n");
			movieDetails.append("\n");
			movieDetails.append("YEAR: "+responseObject.getInt("year"));
			movieDetails.append("\n");
			movieDetails.append("\n");
			movieDetails.append("RUNTIME: "+responseObject.getInt("runtime")+" Minutes");
			movieDetails.append("\n");
			movieDetails.append("\n");
			if(genres.length()>0){
				movieDetails.append("GENRE: ");
				for(int i = 0; i<genres.length();i++){
					movieDetails.append(genres.getString(i));
					if(i<genres.length()-1){
						movieDetails.append(",");
					}
				}
				movieDetails.append("\n");
				movieDetails.append("\n");
			}
			if(responseObject.getString("synopsis").length()>0){
				movieDetails.append("DESCRIPTION: ");
				movieDetails.append(responseObject.getString("synopsis"));
				movieDetails.append("\n");
				movieDetails.append("\n");
			}
			JSONArray cast = responseObject.getJSONArray("abridged_cast");
			if(cast.length()>0){
				movieDetails.append("CAST: ");
				for(int i = 0; i<cast.length(); i++){
					movieDetails.append(cast.getJSONObject(i).getString("name"));
					if(i<cast.length()-1){
						movieDetails.append(",");
					}
				}
			}
			movieDetails.append("\n");
			movieDetails.append("\n");
			JSONArray directors = responseObject.getJSONArray("abridged_directors");
			if(directors.length()>0){
				movieDetails.append("DIRECTOR: ");
				for(int i = 0; i<directors.length(); i++){
					movieDetails.append(directors.getJSONObject(i).getString("name"));
					if(i<directors.length()-1){
						movieDetails.append(",");
					}
				}
			}
			movieDetails.append("\n");
			movieDetails.append("\n");
			String rtLink = responseObject.getJSONObject("links").getString("alternate");
			movieDetails.append("FOR MORE INFO: ");
			movieDetails.append("\n");
			movieDetails.append(rtLink);
			movieDetails.append("\n");
			currentMovieObject.setBody(movieDetails.toString());
			imageUrl = responseObject.getJSONObject("posters").getString("detailed");
		}
		catch(JSONException e){}
		Bitmap bitmap = null;
		BitmapFactory.Options options = new BitmapFactory.Options();
		HttpURLConnection connection = null;
		InputStream in = null;
		try{
			URL url = new URL(imageUrl);
			connection = (HttpURLConnection)url.openConnection();
			if(connection.getResponseCode()!=HttpURLConnection.HTTP_OK){
				return null;
			}
			in = new BufferedInputStream(connection.getInputStream());
			options.inJustDecodeBounds = true;
			bitmap = BitmapFactory.decodeStream(in, null, options);
			options.inSampleSize = calculateInSampleSize(options, activity.getImageViewWidth(), activity.getImageViewHeight());
			options.inJustDecodeBounds = false;
			in.reset();
			bitmap = BitmapFactory.decodeStream(in, null, options);
		}
		catch(Exception e){}
		finally{
			if(in!=null){
				try{
					in.close();
				}
				catch(IOException e){}
				if(connection!=null){
					connection.disconnect();
				}
			}
		}
		currentMovieObject.setImage(bitmap);
		return currentMovieObject;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		progressDialog = new ProgressDialog(activity);
		progressDialog.setMessage("Loading data...");
		progressDialog.setIndeterminate(true);
		progressDialog.setCanceledOnTouchOutside(false);
		progressDialog.show();
	}

	@Override
	protected void onPostExecute(GetMovieByIdTask.MovieObject result) {
		super.onPostExecute(result);
		if(result!=null){
			activity.processMovieObjectInfo(result);
			progressDialog.dismiss();
			
		}
		else{
			progressDialog.dismiss();
			activity.showErrorMessage();
		}
	}
	
	
	/////////////////////////////////////////////////////////////////////////////////////
	// This object contains all the current movie data. will return to the EditMovieActivity for processing.
	public class MovieObject{
		
		private String subject;
		private String body;
		private Bitmap image;
		
		public MovieObject(){
			
		}
		
		
		public void setSubject(String subject){
			this.subject = subject;
		}
		
		
		public String getSubject(){
			return subject;
		}
		
		
		public void setBody(String body){
			this.body = body;
		}
		
		
		public String getBody(){
			return body;
		}

		
		public void setImage(Bitmap image){
			this.image = image;
		}
		
		
		public Bitmap getImage(){
			return image;
		}
	}
	
	
	private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
	    int height = options.outHeight;
	    int width = options.outWidth;
	    int inSampleSize = 1;
	    if (height > reqHeight || width > reqWidth) {
	    	// Calculate ratios of height and width to requested height and width
	        final int heightRatio = Math.round((float) height / (float) reqHeight);
	        final int widthRatio = Math.round((float) width / (float) reqWidth);
	        
	        inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
	    }
	    return inSampleSize;
	}
	
	
	

	

}
