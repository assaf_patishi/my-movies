package assafPatishi.MyMovies.UI;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import assafPatishi.MyMovies.Data.Movie;
import assafPatishi.MyMovies.Data.MoviesHandler;
import static assafPatishi.MyMovies.Data.MoviesDbConstants.*;


public class MainActivity extends ListActivity implements OnItemClickListener{
	
	private int screenWidth;
	public static MainActivity activity;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		screenWidth = metrics.widthPixels;
		MoviesHandler handler = new MoviesHandler(this);
		Cursor cursor = handler.getAllMovies();
		this.startManagingCursor(cursor); //Although deprecated,the newer approach of using a CursorLoader to manage the cursor requires minimum API version 11.
		showMovies(cursor);								//checking by the documents,this method should still work,but is inefficient because it
		ListView lv = getListView();					//reQueries the cursor (on the UI thread) ,and that may overload the main UI thread if doing some heavy database query operations.
		lv.setOnItemClickListener(this);				//Also,the use of Cursor Loaders requires a drastic change in code due to the need of inheriting from Fragment-Activity (ListFragement)
		registerForContextMenu(lv);						//and the use of the relevant methods for that purpose.
		activity = this;

	}
	
	
	protected void onResume(){
		super.onResume();
		CustomCursorAdapter adapter = (CustomCursorAdapter)this.getListAdapter();
		adapter.notifyDataSetChanged();
		TextView total = (TextView)findViewById(R.id.total);
		total.setText("("+getListView().getCount()+")");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	
	public boolean onPrepareOptionsMenu(Menu menu){
		ListView lv = getListView();
		if(lv.getCount()==0){      
			menu.getItem(1).setVisible(false); //when the list is empty,the "delete all items" and "search" options are invisible
			menu.getItem(2).setVisible(false); 
		}
		else{
			menu.getItem(1).setVisible(true);
			menu.getItem(2).setVisible(true);
		}
		return true;
	}
	
	
	public boolean onOptionsItemSelected(MenuItem item){
		switch(item.getItemId()){
		case R.id.delete_all_items:
			deleteAllItems();
			return true;
		case R.id.search_movies:
			Intent intent = new Intent(this,TransparentActivity.class);
			startActivity(intent);
			return true;
		case R.id.exit:
			finish();
		}
		return super.onOptionsItemSelected(item);	
	}
	
	
	public int getScreenWidth(){
		return screenWidth;
	}
	
	
	public void onButtonClick(View view){
		//Click listener for the buttons
		ImageButton b = (ImageButton)view;
		switch(b.getId()){
		case R.id.options:
			 openOptionsMenu();
			 break;
		case R.id.add_item_btn:
			final String[] items = {"Add manually","Search the Internet"};
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("New Movie");
			builder.setItems(items, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int item) {
					switch(item){
					case 0:
						Intent intent = new Intent(getApplicationContext(), EditMovieActivity.class);
						intent.putExtra("addNewMode", true);
						startActivity(intent);
						overridePendingTransition(R.anim.animation, R.anim.animation2);
						break;
					case 1:
						Intent intent2 = new Intent(getApplicationContext(), MovieSearchActivity.class);
						startActivity(intent2);
					}
				}
			});
			AlertDialog dialog = builder.create();
			dialog.show();
		}
	}

	
	
	private void showMovies(Cursor cursor){  
		//Initiating a new cursor adapter and setting it to work with the listView. Executed in onCreate()
		CustomCursorAdapter adapter = new CustomCursorAdapter(getApplicationContext(),cursor,this);
		this.setListAdapter(adapter);
		TextView total = (TextView)findViewById(R.id.total);
		total.setText("("+getListView().getCount()+")");
	}
	
	
	public void refreshList(){
		//Refreshing the list by setting a new cursor with the adapter. for some reason notifyDataSetChanged() doesn't work when trying to refresh the list when still inside the activity in some situations.
		// (..like after deleting an item).
		MoviesHandler handler = new MoviesHandler(this);
		Cursor cursor = handler.getAllMovies();
		this.startManagingCursor(cursor);
		showMovies(cursor);

	}
	
	  
	
	public void deleteAllItems(){ 
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
		final SharedPreferences.Editor editor = preferences.edit();
		final MoviesHandler handler = new MoviesHandler(this);
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle("Delete");
		alertDialog.setMessage("All items will be deleted"+"\n"+"continue?");
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener(){
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Cursor cursor = handler.getAllMovies(); 
				while(cursor.moveToNext()){          //Delete all the images if uri address != null
					if(cursor.getString(3)!=null){
						deleteFile(cursor.getString(3));
					}
					editor.remove(String.valueOf(cursor.getInt(0))); //Remove unnecessary preferences settings
					editor.remove("movieRating"+String.valueOf(cursor.getInt(0)));
				}
				editor.commit();
				try{
					handler.deleteAllMovies();   //...then,delete the data from the database
				}
				catch(SQLiteException ex){
					Toast toast = Toast.makeText(getApplicationContext(), "Failed delete items", Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
				refreshList();
			}
		});
		alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		alertDialog.show();
			
	}
	
	
	
	public void deleteSingleItem(int id){ 
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
		final SharedPreferences.Editor editor = preferences.edit();
		final int itemId = id;
		final MoviesHandler handler = new MoviesHandler(this);
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle("Delete");
		alertDialog.setMessage("Delete this item?");
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener(){
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Movie movie = new Movie();
				movie.setId(itemId);
				if(movie.getUri()!=null){
					deleteFile(movie.getUri());
				}
				try{
					handler.deleteMovie(movie);
					editor.remove(String.valueOf(itemId));
					editor.remove("movieRating"+String.valueOf(itemId));
					editor.commit();
				}
				catch(SQLiteException ex){
					Toast toast = Toast.makeText(getApplicationContext(), "Failed delete item", Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
				refreshList();
			}
		});
		alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		alertDialog.show();
	}

	
	@Override
	public void onItemClick(AdapterView<?> adView, View target, int position, long id) {
		MoviesHandler handler = new MoviesHandler(this);
		Intent intent = new Intent(this, EditMovieActivity.class);
		intent.putExtra("addNewMode", false);
		String[] extras = new String[4];
		Movie movie = handler.getMovie((int)id);
		extras[0] = movie.getSubject();
		extras[1] = movie.getBody();
		extras[2] = movie.getUri();
		extras[3] = String.valueOf(movie.getId());
		intent.putExtra("extras", extras);
		startActivity(intent);
		overridePendingTransition(R.anim.animation, R.anim.animation2);
		
	}
	
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View view, ContextMenuInfo menuInfo){
		super.onCreateContextMenu(menu, view, menuInfo);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.list_context_menu, menu);
	}
	
	
	
	@Override
	public boolean onContextItemSelected(MenuItem item){
		AdapterContextMenuInfo info = (AdapterContextMenuInfo)item.getMenuInfo();
		MoviesHandler handler = new MoviesHandler(this);
		switch(item.getItemId()){
			case R.id.context_menu_edit:
				Intent intent = new Intent(this, EditMovieActivity.class);
				intent.putExtra("addNewMode", false);
				String[] extras = new String[4];
				Movie movie = handler.getMovie((int)info.id);
				extras[0] = movie.getSubject();
				extras[1] = movie.getBody();
				extras[2] = movie.getUri();
				extras[3] = String.valueOf(movie.getId());
				intent.putExtra("extras", extras);
				startActivity(intent);
				return true;
			case R.id.context_menu_delete:
				deleteSingleItem((int)info.id);
				return true;
			case R.id.context_menu_mark_unmark:
				//Mark the current list item with a green tick mark (generally for mark that movie as "watched").
				//The state will be stored in the default SharedPreferences .
				SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
				SharedPreferences.Editor editor = preferences.edit();
				Movie movie2 = handler.getMovie((int)info.id);
				ImageView mark = (ImageView)info.targetView.findViewById(R.id.watched_mark);
				if(mark.getVisibility()==View.INVISIBLE){
					mark.setVisibility(View.VISIBLE);
					editor.putBoolean(String.valueOf(movie2.getId()), true);
				}
				else{
					mark.setVisibility(View.INVISIBLE);
					editor.putBoolean(String.valueOf(movie2.getId()), false);
				}
				editor.commit();
				return true;
			case R.id.context_menu_rate:
				Dialog dialog = new Dialog(this);
				LayoutInflater inflater = (LayoutInflater)getSystemService(LAYOUT_INFLATER_SERVICE); //Inflating the layout for the rating seekBar
				View layout = inflater.inflate(R.layout.seekbar_dialog,(ViewGroup)findViewById(R.id.dialog_seekbar_layout));
				dialog.setContentView(layout);
				SeekBar seekBar = (SeekBar)layout.findViewById(R.id.seek_bar);
				seekBar.incrementProgressBy(1);
			    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
			    lp.copyFrom(dialog.getWindow().getAttributes());
			    lp.width = WindowManager.LayoutParams.MATCH_PARENT;
			    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
				Movie movie3 = handler.getMovie((int)info.id);
				SharedPreferences preferences2 = PreferenceManager.getDefaultSharedPreferences(this);
				seekBar.setProgress(preferences2.getInt("movieRating"+String.valueOf(movie3.getId()), 0));
				final TextView seekBarValue = (TextView)layout.findViewById(R.id.seekbar_text);
				if(seekBar.getProgress()==0){
					seekBarValue.setText("Not rated");
				}
				else{
					seekBarValue.setText(String.valueOf(seekBar.getProgress()));
				}
				seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
					@Override
					public void onStopTrackingTouch(SeekBar seekBar) {
						
					}
					@Override
					public void onStartTrackingTouch(SeekBar seekBar) {

					}
					@Override
					public void onProgressChanged(SeekBar seekBar, int progress,
							boolean fromUser) {
						if(progress==0){
							seekBarValue.setText("Not rated");
						}
						else{
							seekBarValue.setText(String.valueOf(progress));
						}
					}
				});
				Button saveRatingBtn = (Button)layout.findViewById(R.id.save_rating_btn);
				saveRatingBtn.setOnClickListener(new seekBarButtonsClickListener(dialog,seekBar,(int)info.id));
				Button cancelRatingBtn = (Button)layout.findViewById(R.id.cancel_rating_btn);
				cancelRatingBtn.setOnClickListener(new seekBarButtonsClickListener(dialog,seekBar,(int)info.id));
				dialog.show();
				dialog.getWindow().setAttributes(lp);
				return true;
			case R.id.context_menu_share:
				Movie sharedMovie = handler.getMovie((int)info.id);
				Intent shareIntent = new Intent(Intent.ACTION_SEND);
				shareIntent.setType("text/plain");
				shareIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
				shareIntent.putExtra(Intent.EXTRA_TEXT, sharedMovie.getSubject()+"\n"+sharedMovie.getBody());
				startActivity(Intent.createChooser(shareIntent, "Share with"));
				return true;
		}
		return super.onContextItemSelected(item);
	}
	
	
	
	private class seekBarButtonsClickListener implements OnClickListener{ 
		
		private Dialog dialog;
		private SeekBar seekBar;
		private int movieId;
		
		public seekBarButtonsClickListener(Dialog dialog,SeekBar seekBar,int movieId){
			this.dialog = dialog;
			this.seekBar = seekBar;
			this.movieId = movieId;
		}

		@Override
		public void onClick(View v) {
			Button b = (Button)v;
			switch(b.getId()){
			case R.id.save_rating_btn:
				SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
				SharedPreferences.Editor editor = preferences.edit();
				MoviesHandler handler = new MoviesHandler(getApplicationContext());
				Movie movie = handler.getMovie(movieId);
				editor.putInt("movieRating"+String.valueOf(movie.getId()), seekBar.getProgress());
				editor.commit();
				CustomCursorAdapter adapter = (CustomCursorAdapter)getListView().getAdapter();
				adapter.notifyDataSetChanged();
				dialog.dismiss();
				break;
			case R.id.cancel_rating_btn:
				dialog.dismiss();
			}
			
		}
		
	}


}
