package assafPatishi.MyMovies.UI;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;

public class AutoCompletionTask extends AsyncTask<String,Void,ArrayList<String>>{
	
	private final String rtUrl = "http://api.rottentomatoes.com/api/public/v1.0/movies.json?apikey=fvttwvswhe5sfvpu4cycrfs8&q=";
	//private final String pageLimit = "&page_limit=8";
	private MovieSearchActivity activity;
	
	public AutoCompletionTask(MovieSearchActivity activity){
		this.activity = activity;
	}

	@Override
	protected ArrayList<String> doInBackground(String... arg0) {
		ArrayList<String>titles = new ArrayList<String>();
		String response = sendSearchRequest(arg0[0]);
		try{
			JSONObject responseObject = new JSONObject(response);
			JSONArray movies = responseObject.getJSONArray("movies");
			if(movies.length()==0){
				return null;
			}
			for(int i = 0; i<movies.length(); i++){
				JSONObject movie = movies.getJSONObject(i);
				String title = movie.getString("title");
				titles.add(title);
			}
		}
		catch(JSONException e){}
		return titles;
		
	}

	@Override
	protected void onPostExecute(ArrayList<String>result) {
		super.onPostExecute(result);
		if(result!=null){
			activity.setTxtSearchAdapter(result);
		}
	}
	
	
	private String sendSearchRequest(String searchString){
		BufferedReader input = null;
		HttpURLConnection httpCon = null;
		StringBuilder response = new StringBuilder();
		try{
			String query = URLEncoder.encode(searchString, "utf-8");
			URL url = new URL(rtUrl + query );
			httpCon = (HttpURLConnection)url.openConnection();
			if(httpCon.getResponseCode()!=HttpURLConnection.HTTP_OK){
				return null;
			}
			input = new BufferedReader(new InputStreamReader(httpCon.getInputStream()));
			String line;
			while((line = input.readLine())!=null){
				response.append(line + "\n");
			}
		}
		catch(Exception e){}
		finally{
			if(input!=null){
				try{
					input.close();
				}
				catch(IOException e){}
			}
			if(httpCon!=null){
				httpCon.disconnect();
			}
		}
		return response.toString();
			
	}
	
	

}
