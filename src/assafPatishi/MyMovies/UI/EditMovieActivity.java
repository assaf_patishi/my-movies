package assafPatishi.MyMovies.UI;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.text.util.Linkify;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import assafPatishi.MyMovies.Data.Movie;
import assafPatishi.MyMovies.Data.MoviesHandler;

public class EditMovieActivity extends Activity{
	private EditText subjectEdit;
	private EditText bodyEdit;
	private EditText urlEdit;
	private ImageView imageEdit;
	private DownloadImageTask downloadTask;
	private final String FILE_NAME = "movieImage.png";
	private Bitmap currentBitmap;
	private Intent intent;
	private GetMovieByIdTask getMovieByIdTask;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_movie);
		intent = getIntent();
		subjectEdit = (EditText)findViewById(R.id.subject_edit);
		bodyEdit = (EditText)findViewById(R.id.body_edit);
		urlEdit = (EditText)findViewById(R.id.url_edit);
		imageEdit = (ImageView)findViewById(R.id.image_edit);
		if(!intent.getBooleanExtra("addNewMode", true)&&!intent.getBooleanExtra("internetMode", false)){
			setActivityForEditMode();
		}
		else if(intent.getBooleanExtra("internetMode", false)){
			getMovieByIdTask = new GetMovieByIdTask(this);
			getMovieByIdTask.execute(intent.getLongExtra("itemId", 0));
		}
		Linkify.addLinks(bodyEdit, Linkify.WEB_URLS); //linking web url's in the  body edit text
		
	}
	
	//Returns the imageView (the movie image) dimensions for resizing purposes (AsyncTasks extended classes)
	public int getImageViewHeight(){ 
		return imageEdit.getHeight();
	}
	
	
	public int getImageViewWidth(){
		return imageEdit.getWidth();
	}
	


	public void onButtonClick(View view){
		Button b = (Button)view;
		final MoviesHandler handler = new MoviesHandler(this);
		switch(b.getId()){
		case R.id.show_image_btn:
			if(!TextUtils.isEmpty(urlEdit.getText().toString().trim())){
				downloadTask = new DownloadImageTask(this);
				downloadTask.execute(urlEdit.getText().toString());
			}

			break;
			////////////////////////////////////////////////////////////
		case R.id.save_edit:
			if(intent.getBooleanExtra("addNewMode",true)){ 
				if(TextUtils.isEmpty(subjectEdit.getText().toString().trim())){ //checking if the subject field is empty.
					Toast toast = Toast.makeText(this, "Please enter a subject", Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
					break;
				}
				else if(findMovieDuplication(subjectEdit.getText().toString())){ //Checks if this movie already exists (by subject)
					AlertDialog alertDialog = new AlertDialog.Builder(this).create();
					alertDialog.setMessage("'"+subjectEdit.getText().toString().trim()+"'"+" already exists. Do you want to overwrite it?");
					alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener(){
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							overWriteMovie(getMovieId(subjectEdit.getText().toString()),handler);
						}
					});
					alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE,"NO", new DialogInterface.OnClickListener(){
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
					alertDialog.show();
				}
				/*else if(subjectEdit.getText().toString().length()>60){   //This was considered...
					Toast toast = Toast.makeText(this, "Maximum length is 60 chars", Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
			    	break;
				}*/
				else{
					//Saving a new movie
					saveNewMovie(handler);
					
					
				}
			}
			
			// Updating a movie
			else if(!intent.getBooleanExtra("addNewMode",true)){
				if(TextUtils.isEmpty(subjectEdit.getText().toString().trim())){ 
					Toast toast = Toast.makeText(this, "Please enter a subject", Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
					break;
				}
				else{
					updateMovie(handler);
				}
				
			}
			break;
			/////////////////////////////////////////////////////////////
	   		case R.id.cancel_edit:
	   		if(intent.getBooleanExtra("addNewMode", true)){ //In "add new mode",the user will be promped before finishing/canceling the activity. the boolean value is recieved by the calling intent
	   			if(!TextUtils.isEmpty(subjectEdit.getText().toString().trim())||!TextUtils.isEmpty(bodyEdit.getText().toString().trim())
	   					||!TextUtils.isEmpty(urlEdit.getText().toString().trim())||imageEdit.getDrawable()!=null){
	   				AlertDialog alertDialog = new AlertDialog.Builder(this).create();
	   				alertDialog.setTitle("Cancel");
	   				alertDialog.setMessage("Sure you want to cancel?");
	   				alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "YES" ,new DialogInterface.OnClickListener(){
	   					@Override
	   					public void onClick(DialogInterface dialog, int which) {
	   						finish();
	   					}
	   				});
	   				alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "NO", new DialogInterface.OnClickListener() {
	   					@Override
	   					public void onClick(DialogInterface dialog, int which) {
	   						dialog.dismiss();
	   					}
	   				});
	   				alertDialog.show();
	   			}
	   			else{
	   				finish();
	   			}
	   		}
	   		else{
	   			finish();
	   		}
	   		break;
	   		case R.id.remove_image:
	   			if(imageEdit.getDrawable()!=null){
	   				imageEdit.setImageDrawable(null);
	   			}
	   		}
		
	}
	
	
	private void setActivityForEditMode(){ //preparing the views for the activity
		subjectEdit.setText(intent.getStringArrayExtra("extras")[0]);
		bodyEdit.setText(intent.getStringArrayExtra("extras")[1]);
		if(intent.getStringArrayExtra("extras")[2]!=null){
			InputStream in = null;
			try{
				in = openFileInput(intent.getStringArrayExtra("extras")[2]);
				Bitmap bitmap = BitmapFactory.decodeStream(in);
				imageEdit.setImageBitmap(bitmap);
				currentBitmap = bitmap;
			}
			catch(FileNotFoundException e){}
			finally{
				try{
					in.close();
				}
				catch(IOException e){}
			}
		}
		
	}

	
	
	public void showDownloadErrorMessage(){
		Toast toast = Toast.makeText(this, "Failed downloading image", Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
	}
	
	
	
	public void showErrorMessage(){
		Toast toast = Toast.makeText(this, "Failed processing movie data", Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
	}
	
	
	public void processMovieObjectInfo(GetMovieByIdTask.MovieObject movieObject){
		subjectEdit.setText(movieObject.getSubject());
		bodyEdit.setText(movieObject.getBody());
		imageEdit.setImageBitmap(movieObject.getImage());
		Linkify.addLinks(bodyEdit, Linkify.WEB_URLS);
		currentBitmap = movieObject.getImage();
	}
	

	
	public void setImage(Bitmap image){
		imageEdit.setImageBitmap(image);
		currentBitmap = image;
	}
	
	
	public void saveImageToInternalMemory(Bitmap bitmap,String uri){
		FileOutputStream fos = null;
		try{
			fos = openFileOutput(uri, Context.MODE_PRIVATE);
			bitmap.compress(CompressFormat.PNG, 100, fos);  
		}
		catch(FileNotFoundException e){}
		finally{	
			try{
				fos.close();
			}
			catch(IOException e){}
		}
	}
	
	
	private void saveNewMovie(MoviesHandler handler){
		Movie movie = new Movie();
		movie.setSubject(subjectEdit.getText().toString());
		if(!TextUtils.isEmpty(bodyEdit.getText().toString().trim())){
			movie.setBody(bodyEdit.getText().toString());
		}
		if(imageEdit.getDrawable()!=null){ 
			BitmapDrawable drawable = (BitmapDrawable) imageEdit.getDrawable();
			currentBitmap = drawable.getBitmap();
			String uri = System.currentTimeMillis()+FILE_NAME;
			movie.setUri(uri);
			saveImageToInternalMemory(currentBitmap, uri);
		}
		try{
			handler.addMovie(movie);
		}
		catch(SQLiteException ex){
			Toast toast = Toast.makeText(this, "Failed saving data", Toast.LENGTH_LONG);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
			return;
		}
		Toast toast = Toast.makeText(this, "Saved", Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
		finish();
	}
	
	
	private void updateMovie(MoviesHandler handler){
		Movie movie = new Movie();
		movie.setSubject(subjectEdit.getText().toString());
		movie.setBody(bodyEdit.getText().toString());
		
		if(intent.getStringArrayExtra("extras")[2]!=null&&imageEdit.getDrawable()!=null){
			deleteFile(intent.getStringArrayExtra("extras")[2]); //Delete the previews image and replacing it with the current one,saving to the same uri.
			saveImageToInternalMemory(currentBitmap, intent.getStringArrayExtra("extras")[2]);
			movie.setUri(intent.getStringArrayExtra("extras")[2]);
		}
		else if(intent.getStringArrayExtra("extras")[2]==null&&imageEdit.getDrawable()!=null){
			BitmapDrawable drawable = (BitmapDrawable) imageEdit.getDrawable();
			currentBitmap = drawable.getBitmap();
			String uri = System.currentTimeMillis()+FILE_NAME;
			movie.setUri(uri);
			saveImageToInternalMemory(currentBitmap, uri);
		}
		else if(intent.getStringArrayExtra("extras")[2]!=null&&imageEdit.getDrawable()==null){
			deleteFile(intent.getStringArrayExtra("extras")[2]);
			movie.setUri(null);
		}
		
		movie.setId(Integer.parseInt(intent.getStringArrayExtra("extras")[3]));
		try{
			handler.updateMovie(movie);
		}
		catch(SQLiteException ex){
			Toast toast = Toast.makeText(this, "Failed saving data", Toast.LENGTH_LONG);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
			return;
		}
		Toast toast = Toast.makeText(this, "Saved", Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
		finish();
		
	}
	
	
	private boolean findMovieDuplication(String title){ 
		MoviesHandler handler = new MoviesHandler(this);
		Cursor cursor = handler.getAllMovies();
		while(cursor.moveToNext()){
			if(cursor.getString(1).trim().equals(title.trim())){
				return true;
			}
		}
		return false;
	}
	
	
	private int getMovieId(String title){  
		MoviesHandler handler = new MoviesHandler(this);
		Cursor cursor = handler.getAllMovies();
		while(cursor.moveToNext()){
			if(cursor.getString(1).trim().equals(title.trim())){
				return cursor.getInt(0);
			}
		}
		return -1;
	}
	
	
	private void overWriteMovie(int id,MoviesHandler handler){ //overwriting an existing movie
		Movie movie = handler.getMovie(id);
		movie.setBody(bodyEdit.getText().toString());
		
		if(movie.getUri()!=null&&imageEdit.getDrawable()!=null){
			deleteFile(movie.getUri()); 
			BitmapDrawable drawable = (BitmapDrawable) imageEdit.getDrawable();
			currentBitmap = drawable.getBitmap();
			saveImageToInternalMemory(currentBitmap, movie.getUri());
			
		}
		else if(movie.getUri()==null&&imageEdit.getDrawable()!=null){
			BitmapDrawable drawable = (BitmapDrawable) imageEdit.getDrawable();
			currentBitmap = drawable.getBitmap();
			String uri = System.currentTimeMillis()+FILE_NAME;
			movie.setUri(uri);
			saveImageToInternalMemory(currentBitmap, uri);
		}
		else if(movie.getUri()!=null&&imageEdit.getDrawable()==null){
			deleteFile(movie.getUri());
			movie.setUri(null);
		}
		
		
		try{
			handler.updateMovie(movie);
		}
		catch(SQLiteException ex){
			Toast toast = Toast.makeText(this, "Failed saving data", Toast.LENGTH_LONG);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
			return;
		}
		Toast toast = Toast.makeText(this, "Saved", Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
		finish();
		
	}
	
	

		
		
}
	
	
	
	
	


